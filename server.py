//server-code
import socket
import RPi.GPIO as gp
led=12
gp.setwarnings(False)
gp.setmode(gp.BOARD)
gp.setup(led,gp.OUT)
s=socket.socket()
s.bind(("",5553))
print("binding completed")
s.listen(1)
conn,addr=s.accept()
while 1:
   d=conn.recv(100)
   if d=="1":
      gp.output(led,1)
      print("led on")
   elif d=="0":
      gp.output(led,0)
      print("led off")
   elif d=="Exit":
      print("exit from the process")
   elif d=="kill":
      print("server ends")
   print(d)
s.close()